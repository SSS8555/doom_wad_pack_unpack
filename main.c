#include <stdio.h>
#include "dirent_c.h"

#define WAD_FILES 256
#define WAD_MAXSIZE 1024*512

struct WadHeader
{
	char	Magic[4];
	int32_t NumLumps;
	int32_t Directory;
};

struct WadLump
{
	int32_t FilePos;
	int32_t Size;
	char	Name[8];
};


static struct WadHeader wad_header;
static struct WadLump wad_lumbs[WAD_FILES];
static unsigned char wad_buffer[WAD_MAXSIZE];
static unsigned char newLine[]="\n\r";
static unsigned char saveName[]="data\\XXXXXXXX";

/**h************************************************************************/
extern void printN( int n,unsigned char *str )
{
    for(int i=0;i<n;i++)
    {
        putchar( str[i] );
    }
    return;
}
/***************************************************************************/
static int save_data( char * fname,  char * data, int dataLen )
{

    FILE *write_ptr;

    write_ptr = fopen(fname,"wb");

    fwrite(data,dataLen,1,write_ptr);

    fclose(write_ptr);

    return 0;
}
/***************************************************************************/
static int load_data( char * fname,  char * data, int maxDataLen )
{

    FILE *write_ptr;

    write_ptr = fopen(fname,"rb");

    int readed=fread(data,1,maxDataLen,write_ptr);

    fclose(write_ptr);

    return readed;
}
/**h************************************************************************/
int unwad( char * filename )
{
    FILE *write_ptr;int readed;

    write_ptr = fopen(filename,"rb");

    readed=fread(&wad_header,1,sizeof(struct WadHeader),write_ptr);

    if (fseek (write_ptr, wad_header.Directory, SEEK_SET))
    {
//    	printf("Could not read wad directory");
    	return 1;
    }

    readed=fread(wad_lumbs,wad_header.NumLumps,sizeof(struct WadLump),write_ptr);

    printN(4,wad_header.Magic);printN(2,newLine);


    for(int i=0;i<wad_header.NumLumps;i++)
    {
        printN(8,wad_lumbs[i].Name);
        
//        printf(";size=%i",wad_lumbs[i].Size);
        printN(2,newLine);
    }

    for(int i=0;i<wad_header.NumLumps;i++)
    {
        if (fseek (write_ptr, wad_lumbs[i].FilePos, SEEK_SET))
        {
//           printf("Could not read limb");
           return 2;
        }
        readed=fread(wad_buffer,1,wad_lumbs[i].Size,write_ptr);

        for(int j=0;j<8;j++) {saveName[sizeof(saveName)-9+j]=wad_lumbs[i].Name[j];};
        printN(sizeof(saveName),saveName);printN(2,newLine);

        save_data( saveName,  wad_buffer, wad_lumbs[i].Size );
    }

    fclose(write_ptr);

    return 0;
}
/**h************************************************************************/
int inwad( char * filename )
{

    for(int j=0;j<8;j++) {wad_header.Magic[j]="PWAD"[j];};
    wad_header.NumLumps=0;
    wad_header.Directory=sizeof(struct WadHeader);

    int dataSize=0;

    DIR * theDir;

    theDir = opendir( "data" );
    if( !theDir ) return 1;

    struct dirent *theDirEnt = readdir( theDir );
    while( theDirEnt ) {
        if ( *theDirEnt->d_name != '.') 
        {
//            printf( "  %s\n", theDirEnt->d_name );
            for(int j=0;j<8;j++) {wad_lumbs[wad_header.NumLumps].Name[j]=0;};
            for(int j=0;j<8;j++) 
            {
                 wad_lumbs[wad_header.NumLumps].Name[j]=theDirEnt->d_name[j];
                 if (!theDirEnt->d_name[j]){break;};
            };
            wad_header.NumLumps++;
        }
	theDirEnt = readdir( theDir );
    }
    closedir( theDir );


    for(int i=0;i<wad_header.NumLumps;i++)
    {
        printN(8,wad_lumbs[i].Name);
        for(int j=0;j<8;j++) {saveName[sizeof(saveName)-9+j]=wad_lumbs[i].Name[j];};

        int limbSize=load_data( saveName,  wad_buffer+dataSize, WAD_MAXSIZE-dataSize );

        wad_lumbs[i].FilePos=sizeof(struct WadHeader)+sizeof(struct WadLump)*wad_header.NumLumps+dataSize;
        wad_lumbs[i].Size=limbSize;
        dataSize += limbSize;

//        printf(";size=%i",wad_lumbs[i].Size);
        printN(2,newLine);
    }

    FILE *write_ptr;

    write_ptr = fopen(filename,"wb");

    fwrite(&wad_header,1,sizeof(struct WadHeader),write_ptr);
    fwrite(wad_lumbs,wad_header.NumLumps,sizeof(struct WadLump),write_ptr);
    fwrite(wad_buffer,dataSize,1,write_ptr);

    fclose(write_ptr);

    return 0;
}
/**h************************************************************************/
int main( int argc, char ** argv )
{


    if( argc != 3 ) {
       printf("wad x|a filename\n");
       return 1;
    }

    if( argv[1][0] == 'x' ) {
       unwad(argv[2]);
    }
    else if( argv[1][0] == 'a' ) {
       inwad(argv[2]);
    }
    else {
       printf("Wrong option: '%s'\n", argv[1]);
    }


    return 0;
}
/***************************************************************************/
